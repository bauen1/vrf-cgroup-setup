#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/bpf.h>
#include <net/if.h>
#include <sys/syscall.h>
#include <unistd.h>

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* BPF helpers */
#define BPF_MOV64_REG(dest, src) \
    ((struct bpf_insn) { \
     .code = BPF_ALU64 | BPF_MOV | BPF_X, \
     .dst_reg = dest, \
     .src_reg = src, \
     .off = 0, \
     .imm = 0 \
    })
#define BPF_MOV64_IMM(dest, v) \
    ((struct bpf_insn) { \
     .code = BPF_ALU64 | BPF_MOV | BPF_K, \
     .dst_reg = dest, \
     .src_reg = 0, \
     .off = 0, \
     .imm = v \
    })
#define BPF_STX_MEM(SIZE, DEST, SRC, OFF) \
    ((struct bpf_insn) { \
     .code = BPF_STX | BPF_SIZE(SIZE) | BPF_MEM, \
     .dst_reg = DEST, \
     .src_reg = SRC, \
     .off = OFF, \
     .imm = 0 \
     })

#define BPF_EXIT_INSN() \
    ((struct bpf_insn) { \
     .code = BPF_JMP | BPF_EXIT, \
     .dst_reg = 0, \
     .src_reg = 0, \
     .off = 0, \
     .imm = 0 \
     })

static int bpf_prog_attach_fd(int prog_fd, int target_fd, enum bpf_attach_type type) {
    union bpf_attr attr = {};
    attr.target_fd = target_fd;
    attr.attach_bpf_fd = prog_fd;
    attr.attach_type = type;

    return syscall(__NR_bpf, BPF_PROG_ATTACH, &attr, sizeof(attr));
}

static int prog_load(int idx)
{
    /* from iproute2 */
    struct bpf_insn prog[] = {
        BPF_MOV64_REG(BPF_REG_6, BPF_REG_1),
        BPF_MOV64_IMM(BPF_REG_3, idx),
        BPF_MOV64_IMM(BPF_REG_2,
            offsetof(struct bpf_sock, bound_dev_if)),
        BPF_STX_MEM(BPF_W, BPF_REG_1, BPF_REG_3,
            offsetof(struct bpf_sock, bound_dev_if)),
        BPF_MOV64_IMM(BPF_REG_0, 1), /* r0 = verdict */
        BPF_EXIT_INSN(),
    };

    union bpf_attr attr = {};
    attr.prog_type = BPF_PROG_TYPE_CGROUP_SOCK;
    attr.insns = (uint64_t)(prog);
    attr.insn_cnt = sizeof(prog) / sizeof(struct bpf_insn);
    attr.license = (uint64_t)("GPL");
    attr.prog_ifindex = 0;

    return syscall(__NR_bpf, BPF_PROG_LOAD, &attr, sizeof(attr));
}

int main(int argc, char *argv[]) {
    if (argc <= 1) {
        fprintf(stderr, "usage: %s <vrf name>\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* 1. figure out our own cgroup */
    char path[PATH_MAX];
    snprintf(path, sizeof(path), "/proc/%d/cgroup", getpid());
    FILE *fp = fopen(path, "r");
    if (!fp) {
        perror("error determining cgroup");
        return EXIT_FAILURE;
    }

    char buf[4096];
    int found = 0;
    while (fgets(buf, sizeof(buf), fp)) {
        /* we only want the unified entry */
        if (strstr(buf, "::/") == NULL) {
            continue;
        }

        found = 1;
        break;
    }

    buf[strlen(buf) - 1] = 0;

    fclose(fp);

    if (found == 0) {
        fprintf(stderr, "could not determining cgroup\n");
        return EXIT_FAILURE;
    }

    /* now we need the interface index */
    unsigned int ifx = 0;
    if (strcmp(argv[1], "default") != 0 && strcmp(argv[1], "-") != 0) {
        ifx = if_nametoindex(argv[1]);
        if (ifx == 0) {
            perror("failed to determine interface index");
            return EXIT_FAILURE;
        }
    }

    /* now determine the real path */
    char cgroup_path[PATH_MAX];
    /* TODO: don't hardcode the cgroupv2 mount path ._. */
    snprintf(cgroup_path, sizeof(cgroup_path), "/sys/fs/cgroup/%s", &buf[3]);

    int cg_fd = open(cgroup_path, O_DIRECTORY | O_RDONLY);
    if (cg_fd < 0) {
        snprintf(cgroup_path, sizeof(cgroup_path), "/sys/fs/cgroup/unified%s", &buf[3]);
        cg_fd = open(cgroup_path, O_DIRECTORY | O_RDONLY);
        if (cg_fd < 0) {
            perror("failed to open cgroup");
            return EXIT_FAILURE;
        }
    }

    int prog_fd = prog_load(ifx);
    if (prog_fd < 0) {
        perror("failed to load bpf prog");
        close(cg_fd);
        return EXIT_FAILURE;
    }

    if (bpf_prog_attach_fd(prog_fd, cg_fd, BPF_CGROUP_INET_SOCK_CREATE)) {
        perror("bpf_prog_attach_fd");
        close(cg_fd);
        close(prog_fd);
        return EXIT_FAILURE;
    }

    close(prog_fd);
    close(cg_fd);
    return EXIT_SUCCESS;
}
