CPPFLAGS := -Wall -Wextra
PREFIX := /usr

.PHONY: all
all: vrf-cgroup-setup

.PHONY: clean
clean:
	rm -f vrf-cgroup-setup

.PHONY: install
install:
	install -d $(DESTDIR)$(PREFIX)/libexec
	install vrf-cgroup-setup $(DESTDIR)$(PREFIX)/libexec/
