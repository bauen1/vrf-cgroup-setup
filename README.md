# vrf-cgroup-exec

Helper for systemd services to setup a bpf filter for running "inside" a vrf like `ip vrf exec`

# Example

example-service@.service
```
[Unit]

[Service]

# normally you couldn't do this
CapabilityBoundingSet=
DynamicUser=true
# since `ip vrf exec` requires certain capabilites

ExecStartPre=+/usr/lib/vrf-cgroup-setup %i
ExecStart=example-service
```

# License

GPL-2.0, Some of the code is from iproute2
